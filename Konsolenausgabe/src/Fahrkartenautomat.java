﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {
		double eingezahlterGesamtbetrag;
		double zuZahlenderBetrag;
		while (true) {

			zuZahlenderBetrag = fahrkartenbestellungErfassen();

			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			fahrkartenAusgeben();

			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		}
	}

	public static double fahrkartenbestellungErfassen() {
		double Fahrkartenpreis;
		int Fahrkartenanzahl;
		int Fahrkartenart;

		System.out.print("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n");
		System.out.print("Tageskarte Regeltarif AB [8,60 EUR] (2)\n");
		System.out.print("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n\n");
		System.out.print("Ihre Wahl:");

		Fahrkartenart = tastatur.nextInt();
		Fahrkartenpreis = 0;

		while (Fahrkartenart >= 4 || Fahrkartenart <=0) {
			System.out.print(">>falsche Eingabe<<\n");
			System.out.print("Ihre Wahl:");
			Fahrkartenart = tastatur.nextInt();
		}

		if (Fahrkartenart == 1) {
			Fahrkartenpreis = 2.90;
		}

		if (Fahrkartenart == 2) {
			Fahrkartenpreis = 8.60;
		}

		if (Fahrkartenart == 3) {
			Fahrkartenpreis = 23.50;
		}

		System.out.print("Anzahl der Tickets: ");
		Fahrkartenanzahl = tastatur.nextInt();
		return Fahrkartenanzahl * Fahrkartenpreis;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		double zuZahlenderBetrag = zuZahlen;
		double eingeworfeneMünze;
		double eingezahlterGesamtbetrag = 0.0;

		// Geldeinwurf
		// -----------
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", +(zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag;
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------

			System.out.printf("Der Rückgabebetrag in Höhe von %.2f", rückgabebetrag);
			System.out.printf(" EURO ");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n");

	}
}